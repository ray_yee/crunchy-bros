const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      { path: "", name: "home", component: () => import("pages/Index.vue") },
      {
        path: "orders",
        name: "orders",
        component: () => import("pages/Orders.vue")
      },
      {
        path: "thankyou",
        name: "thankyou",
        component: () => import("pages/ThankYou.vue")
      },
      {
        path: "termsandconditions",
        name: "termsandconditions",
        component: () => import("pages/Terms.vue")
      }
    ]
  }
];

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("pages/Error404.vue")
  });
}

export default routes;
