import Firebase from "@firebase/app";
import "@firebase/database"; // eslint-disable-line
import "@firebase/firestore";

export const fBInit = (config) => {
  return !Firebase.apps.length ? Firebase.initializeApp(config) : Firebase.app();
}
export default !Firebase.apps.length ? Firebase.initializeApp(process.env.FIREBASE) : Firebase.app();
export const db = Firebase.database();
export const fs = Firebase.firestore();
