import Vue from "vue";
import * as Sentry from "@sentry/browser";
import { Vue as VueIntegration } from "@sentry/integrations";

Sentry.init({
  dsn:
    "https://543918626dce45c3b859eeb28f83d3ef@o432014.ingest.sentry.io/5384078",
  integrations: [new VueIntegration({ Vue, attachProps: true })]
});
