import Vue from "vue";
import dayjs from "dayjs";
import "dayjs/locale/en-au";

Vue.prototype.$dayjs = dayjs;
