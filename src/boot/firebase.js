import Firebase from "@firebase/app";
import "@firebase/database"; // eslint-disable-line
import "@firebase/firestore";

const firebase = !Firebase.apps.length
  ? Firebase.initializeApp(
      process.env.DEV ? process.env.FIREBASE : process.env.FIREBASE
    )
  : Firebase.app();

export default ({ Vue }) => {
  // Initialize Firebase from settings

  Vue.prototype.$firebase = firebase;
};

export const db = firebase.database();
export const fs = firebase.firestore();
