import {fBInit} from '../services/firebase'

export default async () => {
  const config = process.env.FIREBASE
  fBInit(config)

}