import Vue from "vue";
import Vuex from "vuex";
import { fs, db } from "../boot/firebase";
import dayjs from "dayjs";
import orderBy from "lodash/orderBy";
import "dayjs/locale/en-au";

// import example from './module-example'

Vue.use(Vuex);

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default function(/* { ssrContext } */) {
  const Store = new Vuex.Store({
    state: {
      food: null,
      menu: null,
      menuCalendar: null,
      order: [],
      orderZone: null,
      postalCodes: [],
      suburbs: [],
      combos: null
    },
    mutations: {
      SET_MENU_CALENDAR(state, payload) {
        state.menuCalendar = payload;
      },
      SET_FOOD(state, payload) {
        state.food = payload;
      },
      SET_COMBOS(state, payload) {
        state.combos = payload;
      },
      SET_MENU(state, payload) {
        state.menu = payload;
      },
      SET_POSTALCODES(state, payload) {
        state.suburbs = payload;
        state.postalCodes = payload.map(i => parseInt(i.postalCode));
      },
      ADD_ORDER(state, { item, date, zone }) {
        state.orderZone = zone;
        if (item.type === "Combo Choice") {
          state.order.push({
            ...item,
            qty: 1,
            date
          });
        } else {
          let similarOrder = state.order.findIndex(i => i.name === item.name);
          if (similarOrder !== -1) {
            state.order[similarOrder].qty += 1;
          } else {
            state.order.push({
              ...item,
              qty: 1,
              date
            });
          }
        }
      },
      REMOVE_ORDER(state, index) {
        if (state.order[index].qty !== 1) {
          state.order[index].qty -= 1;
        } else {
          state.order.splice(index, 1);
        }
      }
    },
    actions: {
      loadFood({ commit }) {
        return new Promise((resolve, reject) => {
          db.ref("food").once("value", snapshot => {
            let val = snapshot.val();
            if (val) {
              commit("SET_FOOD", val);
              resolve(val);
            }
            resolve();
          });
        });
      },
      loadCombos({ commit }) {
        return new Promise((resolve, reject) => {
          db.ref("combos")
            .orderByChild("active")
            .equalTo(true)
            .once("value", snapshot => {
              let val = snapshot.val();
              if (val) {
                commit("SET_COMBOS", val);
                resolve(val);
              }
              resolve();
            })
            .catch(err => {
              resolve();
            });
        });
      },
      loadPostalCodes({ commit }) {
        return new Promise((resolve, reject) => {
          db.ref("postalCodes")
            .orderByChild("active")
            .equalTo(true)
            .once("value", snapshot => {
              let val = snapshot.val();
              if (val) {
                let newVal = [];
                val.forEach(item => {
                  if (item) {
                    newVal.push(item);
                  }
                });
                const sorted = orderBy(newVal, "suburb", "asc");
                commit("SET_POSTALCODES", sorted);
                resolve(sorted);
              }
              resolve();
            });
        });
      },
      loadMenu({ commit }) {
        return new Promise((resolve, reject) => {
          db.ref("menu").once("value", snapshot => {
            let val = snapshot.val();
            if (val) {
              commit("SET_MENU", val);
              resolve(val);
            }
            resolve();
          });
        });
      },
      loadMenuCalendar({ commit }) {
        return new Promise((resolve, reject) => {
          fs.collection("menuCalendar")
            .where("date", ">", dayjs().format("YYYYMMDD"))
            .orderBy("date")
            .onSnapshot(querySnapshot => {
              let menuCalendar = {};
              let count = 0;
              querySnapshot.forEach(doc => {
                const { date, menuKey, show } = doc.data();
                if (count < 2) {
                  if (
                    (dayjs().format("YYYYMMDD") ===
                      dayjs(date)
                        .subtract(1, "day")
                        .format("YYYYMMDD") &&
                      dayjs().hour() >= 18) ||
                    show === false
                  ) {
                  } else {
                    menuCalendar[date] = {
                      date,
                      menuKey,
                      deliveryZones: doc.data().deliveryZones
                        ? doc.data().deliveryZones
                        : []
                    };
                    count++;
                  }
                }
              });
              commit("SET_MENU_CALENDAR", menuCalendar);
              resolve();
            });
        });
      },
      addOrder({ commit }, { paymentDetails, orders }) {
        return new Promise((resolve, reject) => {
          fs.collection("payments")
            .add(paymentDetails)
            .then(async docRef => {
              let promises = [];
              Object.keys(orders).map(date => {
                const orderData = {
                  date,
                  paymentKey: docRef.id,
                  items: orders[date].orders
                };
                let a = fs.collection("orders").add(orderData);
                promises.push(a);
              });
              await Promise.all(promises);
              resolve();
            })
            .catch(function(error) {
              console.error("Error adding document: ", error);
              reject();
            });
        });
      }
    }

    // enable strict mode (adds overhead!)
    // for dev mode only
  });

  return Store;
}
