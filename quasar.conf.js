/*
 * This file runs in a Node context (it's NOT transpiled by Babel), so use only
 * the ES6 features that are supported by your Node version. https://node.green/
 */

// Configuration for your app
// https://quasar.dev/quasar-cli/quasar-conf-js

module.exports = function(/* ctx */) {
  return {
    // https://quasar.dev/quasar-cli/supporting-ts
    supportTS: false,

    // https://quasar.dev/quasar-cli/prefetch-feature
    // preFetch: true,

    // app boot file (/src/boot)
    // --> boot files are part of "main.js"
    // https://quasar.dev/quasar-cli/boot-files
    boot: ["axios", "lazyload", "firebase", "dayjs", "cloudinary", "sentry"],

    preFetch: true,
    // https://quasar.dev/quasar-cli/quasar-conf-js#Property%3A-css
    css: ["app.css", "app.sass"],

    // https://github.com/quasarframework/quasar/tree/dev/extras
    extras: [
      // 'ionicons-v4',
      // 'mdi-v5',
      "fontawesome-v5",
      // 'eva-icons',
      // 'themify',
      // 'line-awesome',
      // 'roboto-font-latin-ext', // this or either 'roboto-font', NEVER both!

      // "roboto-font", // optional, you are not bound to it
      "material-icons" // optional, you are not bound to it
    ],

    // Full list of options: https://quasar.dev/quasar-cli/quasar-conf-js#Property%3A-build
    build: {
      vueRouterMode: "history",
      env: {
        FIREBASE: {
          apiKey: "AIzaSyD8uNAIkoZSXu-iKa3RORylgGVlFb3oUfM",
          authDomain: "crunchy-bros.firebaseapp.com",
          databaseURL: "https://crunchy-bros.firebaseio.com",
          projectId: "crunchy-bros",
          storageBucket: "crunchy-bros.appspot.com",
          messagingSenderId: "669477453153",
          appId: "1:669477453153:web:249ea1ec2783312b1d1920",
          measurementId: "G-BMKCWCWWB2"
        },
        FIREBASE_TEST: {
          apiKey: "AIzaSyB2yiQ0xfVzIpyomr1M5Tp7aj1_rFoz0B8",
          authDomain: "crunchy-bros-dev.firebaseapp.com",
          databaseURL: "https://crunchy-bros-dev.firebaseio.com",
          projectId: "crunchy-bros-dev",
          storageBucket: "crunchy-bros-dev.appspot.com",
          messagingSenderId: "378925967536",
          appId: "1:378925967536:web:7536402a807ed9e5de3375",
          measurementId: "G-W5M92EDBJF"
        },
        STRIPE_KEY: "pk_live_UjGVpD4cUipyRmzYsWwgr7o3006gHn706x",
        STRIPE_TEST_KEY:
          "pk_test_51GszXyC7Sr16L1mV1xLWD4KJr1LxVAMKEMvYsLlcYwOOUW8CY8JC997ZmIDLD3ltSmVipzB3Oc6Lbni9Q5kWsEyx00qgT6WlkD"
      },
      // transpile: false,

      // Add dependencies for transpiling with Babel (Array of string/regex)
      // (from node_modules, which are by default not transpiled).
      // Applies only if "transpile" is set to true.
      // transpileDependencies: [],

      // rtl: false, // https://quasar.dev/options/rtl-support
      // preloadChunks: true,
      // showProgress: false,
      gzip: true,
      // analyze: true,

      // Options below are automatically set depending on the env, set them if you want to override
      extractCSS: true,

      // https://quasar.dev/quasar-cli/handling-webpack
      extendWebpack(cfg) {}
    },

    // Full list of options: https://quasar.dev/quasar-cli/quasar-conf-js#Property%3A-devServer
    devServer: {
      https: false,
      port: 8080,
      open: true // opens browser window automatically
    },

    // https://quasar.dev/quasar-cli/quasar-conf-js#Property%3A-framework
    framework: {
      iconSet: "material-icons", // Quasar icon set
      lang: "en-us", // Quasar language pack
      config: {},

      // Possible values for "importStrategy":
      // * 'auto' - (DEFAULT) Auto-import needed Quasar components & directives
      // * 'all'  - Manually specify what to import
      importStrategy: "auto",

      // Quasar plugins
      plugins: ["Notify", "Loading", "Dialog"]
    },

    // animations: 'all', // --- includes all animations
    // https://quasar.dev/options/animations
    animations: [],

    // https://quasar.dev/quasar-cli/developing-ssr/configuring-ssr
    ssr: {
      pwa: false
    },

    // https://quasar.dev/quasar-cli/developing-pwa/configuring-pwa
    pwa: {
      workboxPluginMode: "GenerateSW", // 'GenerateSW' or 'InjectManifest'
      workboxOptions: {}, // only for GenerateSW
      manifest: {
        name: `Crunchy Bros`,
        short_name: `Crunchy Bros`,
        description: `We are a boutique bento workshop, operating from a council registered kitchen. Creating nostalgic flavours from South East Asia and delivering them in the forms of bentos and rice bowls to all in the South Eastern suburbs. Pre-orders are essential.`,
        display: "standalone",
        orientation: "portrait",
        background_color: "#ffffff",
        theme_color: "#027be3",
        icons: [
          {
            src: "icons/icon-128x128.png",
            sizes: "128x128",
            type: "image/png"
          },
          {
            src: "icons/icon-192x192.png",
            sizes: "192x192",
            type: "image/png"
          },
          {
            src: "icons/icon-256x256.png",
            sizes: "256x256",
            type: "image/png"
          },
          {
            src: "icons/icon-384x384.png",
            sizes: "384x384",
            type: "image/png"
          },
          {
            src: "icons/icon-512x512.png",
            sizes: "512x512",
            type: "image/png"
          }
        ]
      }
    },

    // Full list of options: https://quasar.dev/quasar-cli/developing-cordova-apps/configuring-cordova
    cordova: {
      // noIosLegacyBuildFlag: true, // uncomment only if you know what you are doing
    },

    // Full list of options: https://quasar.dev/quasar-cli/developing-capacitor-apps/configuring-capacitor
    capacitor: {
      hideSplashscreen: true
    },

    // Full list of options: https://quasar.dev/quasar-cli/developing-electron-apps/configuring-electron
    electron: {
      bundler: "packager", // 'packager' or 'builder'

      packager: {
        // https://github.com/electron-userland/electron-packager/blob/master/docs/api.md#options
        // OS X / Mac App Store
        // appBundleId: '',
        // appCategoryType: '',
        // osxSign: '',
        // protocol: 'myapp://path',
        // Windows only
        // win32metadata: { ... }
      },

      builder: {
        // https://www.electron.build/configuration/configuration

        appId: "crunchybros"
      },

      // More info: https://quasar.dev/quasar-cli/developing-electron-apps/node-integration
      nodeIntegration: true,

      extendWebpack(/* cfg */) {
        // do something with Electron main process Webpack cfg
        // chainWebpack also available besides this extendWebpack
      }
    }
  };
};
